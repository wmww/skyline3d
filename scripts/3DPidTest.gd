extends RigidBody

const Orient = preload('res://scripts/orient.gd')
var orient := Orient.new(self, 2.0, 15.0, 6.0)

var target_normal

var normal_list = [
	Vector3(0, 0, -1),
	Vector3(0, 1, 0),
	Vector3(-1, 1, 1)
]
var normal_i = 0
var i = 0

func next_normal():
	target_normal = normal_list[normal_i].normalized()
	normal_i = (normal_i + 1) % normal_list.size()
	orient.set_target_up(target_normal)

func _ready():
	next_normal()
	
func _process(delta):
	var up_vec = Vector3(0, 1, 0)
	if up_vec.dot(target_normal) > 0.9:
		up_vec = Vector3(1, 0, 0)
	get_node("../Normal").look_at(target_normal, up_vec.normalized())
	get_node("../MeshInstance2").global_transform.origin = target_normal * 2.35

func _input(event: InputEvent):
	if event is InputEventMouseButton and event.pressed:
		next_normal()

func _physics_process(delta):
	orient.apply()
	add_torque(Vector3(1, 1, 0))
	i = (i + 1) % 5
