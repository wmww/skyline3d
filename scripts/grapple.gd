extends RigidBody

const WireScene = preload('res://scenes/tess/Wire.tscn')
var wire: Spatial
var source_body: RigidBody
var source_anchor: Spatial
var is_anchored := false
var pull_force := 0.0

func _ready():
	set_use_continuous_collision_detection(true)
	add_collision_exception_with(source_body)
	set_contact_monitor(true)
	set_max_contacts_reported(4)
	wire = WireScene.instance()
	wire.bodies[0] = source_body
	wire.anchor_offsets[0] = source_anchor.translation
	wire.bodies[1] = self
	wire.min_len = 4
	get_tree().root.add_child(wire)
	apply_central_impulse(global_transform.basis[1] * 312)

func set_pull_force(pull):
	pull_force = pull
	if is_anchored:
		wire.set_pull_force(pull)
		#wire.set_ratchet(pull > 0)

func destroy():
	wire.set_pull_force(0)
	wire.hide()
	wire.queue_free()
	hide()
	queue_free()

func _on_RigidBody2_body_entered(body):
	set_mode(RigidBody.MODE_STATIC)
	set_collision_layer_bit(0, false)
	set_linear_velocity(Vector3(0, 0, 0))
	is_anchored = true
	set_pull_force(pull_force)
