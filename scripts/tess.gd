extends RigidBody

signal integrate_forces(state)
const GrappleScene = preload('res://scenes/tess/PhysicsGrapple.tscn')
const Orient = preload('res://scripts/orient.gd')
var search_rays = [
	Vector3(0, -1, 0),
	Vector3(0, 1, 0),
	Vector3(1, 0, 0),
	Vector3(-1, 0, 0),
	Vector3(0, 0, 1),
	Vector3(0, 0, -1),
]

enum ANIM_STATE {
	idle = 0,
	running = 1,
	falling = 2,
}

enum SIDE {
	left = 0,
	right = 1,
}

var stabalizer: Spatial
var tess_body: Spatial
var selfie_stick: RayCast
var camera: Camera
var animation_player: AnimationPlayer

var orient := Orient.new(self, 0.8, 10.0, 2.0)
var tess_normal := Vector3(0, 1, 0)
var tess_state := -1
var last_fast_vel = Vector3(1, 0, 0)
var grapples = [null, null]
var visible_grapples

func _ready():
	stabalizer = $Stabalizer
	selfie_stick = $Stabalizer/SelfieStick
	camera = $Stabalizer/SelfieStick/Camera
	tess_body = $Stabalizer/Tess
	animation_player = $Stabalizer/Tess/Tess/AnimationPlayer
	set_can_sleep(false)
	visible_grapples = [$Stabalizer/Tess/LeftVisibleGrapple, $Stabalizer/Tess/RightVisibleGrapple]
	selfie_stick.set_cast_to(camera.translation)
	selfie_stick.add_exception(self)
	selfie_stick.set_enabled(true)
	set_anim_state(ANIM_STATE.idle)

func set_anim_state(state: int):
	if state == tess_state:
		return
	tess_state = state
	if state == ANIM_STATE.idle:
		animation_player.play("idle-loop", 0.5)
	elif state == ANIM_STATE.running:
		animation_player.play("run-loop", 0.1)
	elif state == ANIM_STATE.falling:
		animation_player.play("fall-loop", 0.2)

func shoot_grapple(side: int, direction: Vector3):
	if grapples[side] != null:
		grapples[side].destroy()
	var grapple = GrappleScene.instance()
	grapples[side] = grapple
	grapple.source_body = self
	grapple.source_anchor = visible_grapples[side]
	grapple.transform = visible_grapples[side].global_transform
	#var selfie_stick_basis = $SelfieStick.global_transform.basis
	#grapple.transform.basis = selfie_stick_basis.rotated(selfie_stick_basis.x, PI * 0.5)
	grapple.transform = grapple.transform.looking_at(
		grapple.transform.origin + direction,
		Vector3(0, 1, 0))
	grapple.rotate(grapple.transform.basis.x, -PI * 0.5)
	grapple.linear_velocity = linear_velocity
	visible_grapples[side].hide()
	get_tree().root.add_child(grapple)

func pan_view(movement: Vector2):
	selfie_stick.rotate(Vector3(0, 1, 0), deg2rad(movement.x))
	selfie_stick.rotate(selfie_stick.transform.basis[0], deg2rad(movement.y))

func grapple_button_pressed(side: int, screen_pos: Vector2):
	shoot_grapple(side, camera.project_ray_normal(screen_pos))
	grapples[side].set_pull_force(250.0)
	
func grapple_button_released(side: int):
	if grapples[side]:
		grapples[side].destroy()
		grapples[side] = null
	visible_grapples[side].show()

func _process(delta):
	stabalizer.global_transform.basis = Basis()
	var vel = linear_velocity
	if vel.length() > 0.5:
		last_fast_vel = vel
	else:
		vel = last_fast_vel
	tess_body.global_transform = tess_body.global_transform.looking_at(
		tess_body.global_transform.origin + tess_normal,
		vel)
	tess_body.rotate(tess_body.transform.basis.x, -PI/2)
	var actions = ["left_grapple", "right_grapple"]
	for side in range(2):
		if Input.is_action_just_pressed(actions[side]):
			grapple_button_pressed(side, get_viewport().get_mouse_position())
		if Input.is_action_just_released(actions[side]):
			grapple_button_released(side)
	var x = Input.get_action_strength("ui_left") - Input.get_action_strength("ui_right")
	var y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	if x != 0 or y != 0:
		pan_view(Vector2(x, y) * 180 * delta)

func _input(event: InputEvent):
	if event is InputEventMouseMotion and (
		Input.is_mouse_button_pressed(BUTTON_LEFT) or
		Input.is_mouse_button_pressed(BUTTON_RIGHT) or 
		Input.is_mouse_button_pressed(BUTTON_MIDDLE)):
		pan_view(event.relative * 0.2 * Vector2(-1, 1))

func _integrate_forces(state):
	emit_signal("integrate_forces", state)

func _physics_process(_delta):
	if selfie_stick.is_colliding():
		camera.global_transform.origin = selfie_stick.get_collision_point()
	else:
		camera.translation = selfie_stick.cast_to
	orient.apply()
	var state = get_world().direct_space_state
	
	# Find the closest intersection
	var hit = null
	var len_squared = 10000
	# TODO: try current normal first
	for ray in search_rays:
		var intersection = state.intersect_ray(global_transform.origin, global_transform.origin + ray * 3, [self])
		if intersection:
			var this_len_squared = (intersection.position - global_transform.origin).length_squared()
			if this_len_squared < len_squared:
				len_squared = this_len_squared
				hit = intersection
	if hit:
		tess_normal = hit.normal
		orient.set_target_up(tess_normal)
		var other_vel = Vector3(0, 0, 0)
		if hit.collider is RigidBody:
			other_vel = hit.collider.linear_velocity
		var rel_vel = linear_velocity - other_vel
		if rel_vel.length() > 2:
			set_anim_state(ANIM_STATE.running)
		else:
			set_anim_state(ANIM_STATE.idle)
	else:
		set_anim_state(ANIM_STATE.falling)
